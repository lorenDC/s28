// CRUD Operations:
/*
	1. Create - insert
	2. Read - find
	3. Update - update
	4. Destroy - delete
*/


// Insert Method () - Create documents in our DB.

/*
	Syntax:
		Insert One Document:
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",
			});

		Insert Many Documents:
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				}
			]);

*/

// upon execution on Robo 3T make sure not to execute your data several times as it will duplicate

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Dela Cruz",
	"age": 21,
	"email": "janedc@ymail.com",
	"company": "none"
})

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawkings",
			"age": 67,
			"email": "Stephenhawkings@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Niel",
			"lastName": "Armstrong",
			"age": 82,
			"email": "nielarmstrong@gmail.com",
			"department": "none"
		}
	]);

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/


db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": true
		}
	]);

// Find Document/ Method - Read

/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match our criteria

		db.collection.findOne({}) - this will return the first document in our collection

*/

db.users.find();

db.users.find({
	"firstName": "Jane"
});

db.users.find({
	"firstName": "Niel",
	"age": 82
})


// Update Documents/Method - Updates our documents in our collection

/*

	updateOne() - updating the first matching document on our collection
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})
	updateMany() - multiple; it updates all the documents that matches our criteria

	Syntax:
		db.collectionName.updateMany(
			{
				"criteria": "value"
			},
			{
				$set: {
					"fieldToBeUpdated": "updatedValue"
				}	
			})

*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
});

// Updating One Document

db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operations",
			"status": "active"
		}
	})

// Removing a field
db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status": "active"
			}
		}
	)

// Updating Multiple Documents
db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}
	)


db.users.updateOne({},
		{
			$set: {
				"department": "Operations"
			}
		}
	)


/*
	Mini Activity: 11:55 AM
		1. In our courses collection, update the HTML 101 course
			- Make the isActive to false
		2. Add enrollees field to all the documents in our courses collection
			-Enrollees: 10
*/

db.courses.updateOne({
	"name": "HTML 101"
},
{
	$set: {
		"isActive": false
	}
});

db.courses.updateOne({},
{
	$set: {
		"Enrollees": 10
	}
});
