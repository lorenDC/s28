// S28 Activity Template:


// 1.) Insert a single room using the updateOne() method.
// Code here:
	db.hotel.insertOne({
	"name": "single",
	"accommodates": 2,
	"proce": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
});

// 2.) Insert multiple rooms using the insertMany() method.
// Code here:

	db.hotel.insertMany([
	{
		"name": "double",
		"accommodates": 4,
		"price": 2000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": true
	},
	{
		"name": "King",
		"accommodates": 6,
		"price": 4000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": true
	},
	{
		"name": "Queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": true
	},
	{
		"name": "Family",
		"accommodates": 8,
		"price": 8000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": true
	}
	]);

// 3.) Use find() method to search for a room with a name "double".
// Code here:
	db.users.find({
	"name": "double"
	});

// 4.) Use the updateOne() method to update the queen room and set the available rooms to 0.
// Code here:
	db.hotel.updateOne(
	{
		"name": "Queen"
	},
	{
		$set: {
			"rooms_available": 0
			"isAvailable": false
		}
	});


// 5.) Use the deleteMany method to delete all rooms that have 0 rooms avaialable.
// Code here:

	db.hotel.deleteMany(
			{
				"rooms_available": 0
			}
		)